from docker.io/library/python:3.11-alpine

RUN mkdir /usr/local/app
WORKDIR /usr/local/app

COPY entrypoint.sh /
ENTRYPOINT /entrypoint.sh

COPY sync-content.sh /usr/local/bin/

RUN apk add --no-cache git make rclone
