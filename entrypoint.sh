#!/bin/sh
set -euo pipefail

[ -d .git ] || git clone --single-branch --depth 1 "$GIT_SOURCE_REPO" . || (git checkout -- * && git pull --depth 1)
if [ -f requirements.txt ] ; then
    [ -d venv ] || python -m venv venv    
    . venv/bin/activate    
    # TODO check if content has changed before installing anything?
    pip install --upgrade pip    
    pip install -r requirements.txt    
    sync-content.sh
    make html
else
	echo 'No requirements.txt found. No python installed or website built.'
fi
