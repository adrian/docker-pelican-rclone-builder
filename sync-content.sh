#!/bin/sh

#set -eu

# Sourc some env
[ -f .env ] && . ./.env

# Used env vars
[ -z "$NC_SHARE_LINK" ] && echo 'Aucun partage cloud trouvé' && exit 1
# NC_SHARE_PASSWORD can be empty

# Extract data from share link
tmp="$(mktemp)"
echo "$NC_SHARE_LINK" | sed 's#/s/# #' > "$tmp"
read server token <"$tmp"
rm "$tmp"

[ -z "$server" ] && echo 'Aucun serveur trouvé dans le lien de partage' && exit 1
[ -z "$token" ] && echo 'Aucun token trouvé dans le lien de partage' && exit 1

# User may ask for sync way
action="$1"

if [ "$action" = 'pull' ] || [ -z "$action" ] ; then
	path=":webdav: $CLOUD_LOCAL_PATH"
elif [ "$action" = 'push' ] ; then
	path="$CLOUD_LOCAL_PATH :webdav:"
else
	echo "unknown action: $action"
	exit 1
fi

# Actual sync
rclone sync --create-empty-src-dirs "--webdav-url=$server/public.php/webdav/" "--webdav-user=$token" --webdav-pass="$(rclone obscure "$NC_SHARE_PASSWORD")" --webdav-vendor=nextcloud $path

echo "SYNC OK"
